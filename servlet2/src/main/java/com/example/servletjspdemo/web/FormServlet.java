package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Make that form</h2>" +
				"<form action='data' method = 'POST'>" +
				"Nick*: <input type='text' name='firstname' required/> <br />" +
				"Password*: <input type='text' name='password' required/> <br />" +
				"Repeat password*: <input type='text' name='rpassword' required/> <br />" +
				"Email*: <input type='text' name='email' required/> <br />" +
				"<input type='submit' value='Register' />" +
				"</form>" +
				"</body></html>");
		out.close();
	}

}
