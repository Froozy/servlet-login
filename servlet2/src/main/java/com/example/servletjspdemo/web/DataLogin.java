package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.servletjspdemo.domain.Person;
import com.example.servletjspdemo.service.StorageService;

@WebServlet(urlPatterns = "/logincheck")
public class DataLogin extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private HttpSession session;

	boolean CheckLogin(String nick,String password)
	{
		int x = StorageService.SearchListLogin(nick);
		if(x == -13)
		{
			return false;
		}
		Person person = new Person();
		person = StorageService.db.get(x);
		if(person.getPassword() == password){
			return true;
		}
		else{
			return false;
		}
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if(CheckLogin(request.getParameter("lnick"),request.getParameter("lpass")))
		{
			session = request.getSession(true);
			session.setAttribute("Member", "lnick");
			doGet(request, response);
	}

}
}
