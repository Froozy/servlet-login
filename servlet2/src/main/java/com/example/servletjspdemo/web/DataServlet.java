package com.example.servletjspdemo.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.servletjspdemo.domain.Person;
import com.example.servletjspdemo.service.StorageService;

@WebServlet(urlPatterns = "/data")
public class DataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private HttpSession session;
	boolean full,mail,again;
	private boolean goodpass;

	
	
	public Person MakePerson(HttpServletRequest request) {

		Person person = new Person();
		person.setNick(request.getParameter("firstname"));
		person.setPassword(request.getParameter("password"));
		person.setRpassword(request.getParameter("rpassword"));
		person.setEmail(request.getParameter("email"));
		return person;
	}
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
				
		boolean goodpass = false;
		Person person = MakePerson(request);
		if(person.getPassword().equals(person.getRpassword()));
		{
			goodpass = true;
		}
		if(goodpass)
		{
			StorageService.add(person);
			request.getRequestDispatcher("NewMember.jsp").forward(request, response);
		}
		else
		{
			request.getRequestDispatcher("WrongForm.jsp").forward(request, response);
		}

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
