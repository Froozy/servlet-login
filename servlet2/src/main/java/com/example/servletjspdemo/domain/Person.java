package com.example.servletjspdemo.domain;

public class Person {
	
	private String nick;
	private String password;
	private String rpassword;
	private String email;
	private int levelaccess = 1; //1-normal 2-premium 3-administrator

	public Person() {
		super();
	}
	
	public Person(String nick,String password,String email) {
		super();
		this.nick = nick;
		this.password = password;
		this.email = email;
	}

	public String getRpassword() {
		return rpassword;
	}

	public void setRpassword(String rpassword) {
		this.rpassword = rpassword;
	}

	public String getNick() {
		return nick;
	}

	public String getPassword() {
		return password;
	}

	public String getEmail() {
		return email;
	}

	public int getLevelaccess() {
		return levelaccess;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLevelaccess(int levelaccess) {
		this.levelaccess = levelaccess;
	}
	
}