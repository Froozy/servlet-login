package com.example.servletjspdemo.service;

import java.util.ArrayList;
import java.util.List;

import com.example.servletjspdemo.domain.Person;

public class StorageService {

	public static List<Person> db = new ArrayList<Person>();
	
	public static void add(Person person){
		db.add(person); 
	}
	
	public static List<Person> getAllPersons(){
		return db;
	}
	public static int SearchListLogin(String name)
	{
		Person person = new Person();
		for(int i=0;i<StorageService.db.size();i++)
		{
			person = StorageService.db.get(i);
			if (person.getNick() == name)
			{
				return i;
			}
		}
		return -13;
	}
}
